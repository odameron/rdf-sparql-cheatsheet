<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="cheat sheet&#xa;RDF-SPARQL" FOLDED="false" ID="ID_608698287" CREATED="1660913531299" MODIFIED="1660913785024" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="7" RULE="ON_BRANCH_CREATION"/>
<node TEXT="legend" POSITION="right" ID="ID_741269028" CREATED="1660925304224" MODIFIED="1660925307573">
<edge COLOR="#00007c"/>
<node TEXT="open in browser" ID="ID_1007922007" CREATED="1660925315217" MODIFIED="1660925325662">
<icon BUILTIN="internet"/>
</node>
<node TEXT="type in terminal" ID="ID_211534485" CREATED="1660925327050" MODIFIED="1660925337484">
<icon BUILTIN="executable"/>
</node>
</node>
<node TEXT="TLDR" POSITION="right" ID="ID_1822711790" CREATED="1660924809787" MODIFIED="1660924813200">
<edge COLOR="#00ffff"/>
<node TEXT="install both jena + fuseki" ID="ID_1713633510" CREATED="1660924814587" MODIFIED="1660924841222">
<node TEXT="https://jena.apache.org/" ID="ID_1578156050" CREATED="1660916769319" MODIFIED="1660916783688">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="small RDF dataset" ID="ID_594217937" CREATED="1660924880235" MODIFIED="1660924885872">
<node TEXT="${JENA_HOME}/bin/sparql --data=/path/to/dataFile --query=/path/to/queryFile" ID="ID_740717796" CREATED="1589242140662" MODIFIED="1660923873016">
<icon BUILTIN="executable"/>
<font NAME="Courier" BOLD="true"/>
</node>
</node>
<node TEXT="medium or large dataset" ID="ID_1562435" CREATED="1660924892020" MODIFIED="1660924904936">
<node TEXT="start your endpoint" ID="ID_284082121" CREATED="1660924947491" MODIFIED="1660924953161">
<node TEXT="${FUSEKI_HOME}/fuseki-server --file=/path/to/RDFfile /serviceName" ID="ID_1896426611" CREATED="1589248132867" MODIFIED="1660922909873">
<icon BUILTIN="executable"/>
<font NAME="Courier" BOLD="true"/>
</node>
<node TEXT="do not close the terminal!" ID="ID_985806152" CREATED="1660923409844" MODIFIED="1660923430750">
<icon BUILTIN="stop-sign"/>
</node>
</node>
<node TEXT="query from the GUI" ID="ID_1831032814" CREATED="1660924972723" MODIFIED="1660925086989">
<node TEXT="http://localhost:3030" ID="ID_310278128" CREATED="1660923371748" MODIFIED="1660923396147">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
</node>
<node TEXT="Create RDF" POSITION="right" ID="ID_1972071383" CREATED="1660913603561" MODIFIED="1660913609032">
<edge COLOR="#ff0000"/>
<node TEXT="turtle syntax (.ttl)" ID="ID_1130774045" CREATED="1660914101097" MODIFIED="1660914127671">
<node TEXT="template" ID="ID_432946712" CREATED="1660915144389" MODIFIED="1660915170220">
<icon BUILTIN="idea"/>
<node TEXT="https://gitlab.com/odameron/rdf-sparql-cheatsheet/-/raw/main/template.ttl" ID="ID_979234678" CREATED="1660915173188" MODIFIED="1660915464744">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="prefixes" ID="ID_1015425969" CREATED="1660915270732" MODIFIED="1660915280714">
<node TEXT="@prefix prefixName: &lt;prefixURL&gt; ." ID="ID_1169345170" CREATED="1660915313157" MODIFIED="1664261857764">
<font NAME="Courier" BOLD="true" STRIKETHROUGH="true"/>
</node>
<node TEXT="PREFIX prefixName: &lt;prefixURL&gt;" ID="ID_972060275" CREATED="1660915313157" MODIFIED="1664261876213">
<font NAME="Courier" BOLD="true"/>
</node>
<node TEXT="usual prefixes" ID="ID_229885204" CREATED="1660915433133" MODIFIED="1660916646788">
<icon BUILTIN="idea"/>
<node TEXT="http://prefix.cc" ID="ID_1724193667" CREATED="1660915493525" MODIFIED="1660915507333">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="IRI" ID="ID_1026929054" CREATED="1660915646910" MODIFIED="1660915649265">
<node TEXT="full IRI" ID="ID_1252800884" CREATED="1660915671165" MODIFIED="1660915675561">
<node TEXT="http://example.org/dir/someIdentifier" ID="ID_146270752" CREATED="1660915739278" MODIFIED="1660915789354">
<font NAME="Courier"/>
</node>
<node TEXT="http://example.org/dir/file#someIdentifier" ID="ID_740434921" CREATED="1660915791973" MODIFIED="1660915818324">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="compact (CURIE)" ID="ID_992502370" CREATED="1660915675845" MODIFIED="1660915688889">
<node TEXT="prefixName:someIdentifier" ID="ID_62330349" CREATED="1660915716166" MODIFIED="1660915733483">
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="datatype value" ID="ID_208007431" CREATED="1660915660429" MODIFIED="1660915666809">
<node TEXT="delimited by simple or double quotes" ID="ID_1843752341" CREATED="1660915858766" MODIFIED="1660915873044">
<node TEXT="&quot;some value&quot;" ID="ID_290460298" CREATED="1660915904414" MODIFIED="1660915950369">
<font NAME="Courier"/>
</node>
<node TEXT="&apos;&apos;some value&apos;" ID="ID_201217517" CREATED="1660915910686" MODIFIED="1660916025385">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="optional language tag" ID="ID_814742829" CREATED="1660915880078" MODIFIED="1660915896445">
<node TEXT="&quot;some value&quot;@en" ID="ID_1181993545" CREATED="1660915955206" MODIFIED="1660915975924">
<font NAME="Courier"/>
</node>
</node>
<node TEXT="optional type" ID="ID_133554405" CREATED="1660915978558" MODIFIED="1660915999809">
<node TEXT="&quot;some value&quot;^^xsd:string" ID="ID_661964742" CREATED="1660916000117" MODIFIED="1660916141774">
<font NAME="Courier"/>
</node>
<node TEXT="&quot;3.14&quot;^^xsd:float" ID="ID_1300608617" CREATED="1660916030734" MODIFIED="1660916141779">
<font NAME="Courier"/>
</node>
<node TEXT="&quot;42&quot;^^xsd:integer" ID="ID_621188684" CREATED="1660916106270" MODIFIED="1660916141780">
<font NAME="Courier"/>
</node>
<node TEXT="&quot;true&quot;^^xsd:boolean" ID="ID_897929737" CREATED="1660916118622" MODIFIED="1660916141780">
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="triple structure" ID="ID_249372337" CREATED="1660916174935" MODIFIED="1660916180666">
<node TEXT="subject: IRI" ID="ID_1591473783" CREATED="1660916181750" MODIFIED="1660916187737"/>
<node TEXT="predicate: IRI" ID="ID_1953612461" CREATED="1660916188894" MODIFIED="1660916195665"/>
<node TEXT="object: IRI or datatype value" ID="ID_917352584" CREATED="1660916196166" MODIFIED="1660916210338"/>
</node>
<node TEXT="standalone triple" ID="ID_991115438" CREATED="1660916238743" MODIFIED="1660916244586">
<node TEXT="subject predicate object ." ID="ID_320704577" CREATED="1660916249319" MODIFIED="1660916470324">
<font NAME="Courier" BOLD="true"/>
</node>
<node TEXT="final dot" ID="ID_1452451311" CREATED="1660916291856" MODIFIED="1660916302974">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node TEXT="triples sharing the same subject" ID="ID_1343195650" CREATED="1660916304855" MODIFIED="1660916439033">
<node TEXT="subject predicate1 object1 ;&#xa;        predicate2 object2 ;&#xa;        predicate3 object3 ." ID="ID_969834865" CREATED="1660916314076" MODIFIED="1660916466408">
<font NAME="Courier" BOLD="true"/>
</node>
<node TEXT="intermediate semicolon" ID="ID_545755656" CREATED="1660916406247" MODIFIED="1660916423349">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="final dot" ID="ID_1652355479" CREATED="1660916415967" MODIFIED="1660916421430">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
<node TEXT="triples sharing the same subject and the same predicate" ID="ID_1150795627" CREATED="1660916478255" MODIFIED="1660916492670">
<node TEXT="subject predicate object1 ,&#xa;                  object2 ,&#xa;                  object3 ." ID="ID_399103809" CREATED="1660916500567" MODIFIED="1660916548773">
<font NAME="Courier" BOLD="true"/>
</node>
<node TEXT="intermediate comma" ID="ID_1483198627" CREATED="1660916406247" MODIFIED="1660923125825">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="final dot" ID="ID_990598178" CREATED="1660916415967" MODIFIED="1660916421430">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
</node>
<node TEXT="validate (optional)" ID="ID_1580787465" CREATED="1660914132842" MODIFIED="1660914138304">
<node TEXT="install apache jena" ID="ID_2814269" CREATED="1660916733237" MODIFIED="1660916738163">
<node TEXT="https://jena.apache.org/" ID="ID_1722696411" CREATED="1660916769319" MODIFIED="1660916783688">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="${JENA_HOME}/bin/riot --validate &lt;filePath&gt;" ID="ID_1727889613" CREATED="1660916813632" MODIFIED="1660916990722">
<icon BUILTIN="executable"/>
<font NAME="Courier" BOLD="true"/>
</node>
</node>
<node TEXT="visualize (optional)" ID="ID_1295045950" CREATED="1660914139018" MODIFIED="1660914153921">
<node TEXT="only for small datasets!" ID="ID_1852725499" CREATED="1660914161306" MODIFIED="1660925448020">
<icon BUILTIN="info"/>
</node>
<node TEXT="install raptor RDF library" ID="ID_1938321882" CREATED="1660917168681" MODIFIED="1660917261793">
<node TEXT="https://librdf.org/raptor/" ID="ID_942858726" CREATED="1660917263712" MODIFIED="1660917280258">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="rapper --input turtle &lt;rdfFilePath&gt; --output dot | dot -Tpng -o $&lt;pngFilePath&gt;" ID="ID_1614679867" CREATED="1660917342329" MODIFIED="1660917378025">
<icon BUILTIN="executable"/>
<font NAME="Courier" BOLD="true"/>
</node>
<node TEXT="simpler interface to rapper" ID="ID_1801135514" CREATED="1660917425200" MODIFIED="1660925455406">
<icon BUILTIN="idea"/>
<node TEXT="https://gitlab.com/odameron/rdf2image" ID="ID_951740081" CREATED="1660917463129" MODIFIED="1660917479290">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
<node TEXT="rdf2image -r &lt;rdfFilePath&gt; -p &lt;pngFilePath&gt;" ID="ID_1204595335" CREATED="1660917484689" MODIFIED="1660922537139">
<icon BUILTIN="executable"/>
<font NAME="Courier" BOLD="true"/>
</node>
</node>
</node>
</node>
<node TEXT="Set up endpoint (optional)" POSITION="right" ID="ID_1896102214" CREATED="1660913609969" MODIFIED="1660913618844">
<edge COLOR="#0000ff"/>
<node TEXT="install apache fuseki" ID="ID_406107152" CREATED="1660921769201" MODIFIED="1660921780031">
<node TEXT="https://jena.apache.org/" ID="ID_140900262" CREATED="1660921799544" MODIFIED="1660921813116">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
<node TEXT="if necessary (hint: almost never), increase heap space" ID="ID_1074326261" CREATED="1661327063679" MODIFIED="1661327093843">
<node TEXT="edit ${FUSEKI_HOME}/fuseki-server" ID="ID_1327112388" CREATED="1661327097168" MODIFIED="1661327146521"/>
<node TEXT="#JVM_ARGS=${JVM_ARGS:--Xmx4G}&#xa;JVM_ARGS=${JVM_ARGS:--Xmx16G}" ID="ID_457698153" CREATED="1661327147175" MODIFIED="1661327202524">
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="alternative 1: load to RAM" ID="ID_1916980761" CREATED="1660921858993" MODIFIED="1660921893389">
<node TEXT="for small to moderate datasets" ID="ID_1012785304" CREATED="1660921895497" MODIFIED="1660925465773">
<icon BUILTIN="info"/>
</node>
<node TEXT="${FUSEKI_HOME}/fuseki-server --file=/path/to/RDFfile /serviceName" ID="ID_1448627526" CREATED="1589248132867" MODIFIED="1660922909873">
<icon BUILTIN="executable"/>
<font NAME="Courier" BOLD="true"/>
</node>
<node TEXT="do not close the terminal!" ID="ID_1962652609" CREATED="1660923409844" MODIFIED="1660923430750">
<icon BUILTIN="stop-sign"/>
</node>
<node TEXT="&quot;serviceName&quot; is the (short) name of your dataset" ID="ID_636760640" CREATED="1589248182467" MODIFIED="1589248425603"/>
<node TEXT="use as many &quot;--file&quot; as necessary" ID="ID_957608991" CREATED="1660922025039" MODIFIED="1660922038429"/>
<node TEXT="use &quot;--update&quot; if the dataset can be modified" ID="ID_1494529582" CREATED="1589248322715" MODIFIED="1589248346114"/>
</node>
<node TEXT="alternative 2: load from disk" ID="ID_1526502252" CREATED="1660922259058" MODIFIED="1660922271409">
<node TEXT="for large datasets" ID="ID_831070988" CREATED="1660922277433" MODIFIED="1660925471777">
<icon BUILTIN="info"/>
</node>
<node TEXT="step1: generate TDB directory" ID="ID_900557801" CREATED="1660922345122" MODIFIED="1660922355685">
<node TEXT="needs to be done only once" ID="ID_935036517" CREATED="1660922409738" MODIFIED="1660922415828"/>
<node TEXT="${JENA_HOME}/bin/tdb1.xloader --loc=/path/to/TDBdirectory file [files]" ID="ID_1703845047" CREATED="1589241891408" MODIFIED="1660922516272">
<icon BUILTIN="executable"/>
<font NAME="Courier" BOLD="true"/>
</node>
</node>
<node TEXT="step2: start fuseki" ID="ID_1263038277" CREATED="1660922357042" MODIFIED="1660922407180">
<node TEXT="${FUSEKI_HOME}/fuseki-server --loc=/path/to/TDBdirectory /serviceName" ID="ID_391690029" CREATED="1589244127158" MODIFIED="1660922519109">
<icon BUILTIN="executable"/>
<font NAME="Courier" BOLD="true"/>
</node>
<node TEXT="do not close the terminal!" ID="ID_646841257" CREATED="1660923409844" MODIFIED="1660923430750">
<icon BUILTIN="stop-sign"/>
</node>
<node TEXT="&quot;serviceName&quot; is the (short) name of your dataset" ID="ID_1936628986" CREATED="1589244203559" MODIFIED="1589248439944"/>
<node TEXT="use &quot;--update&quot; if the dataset can be modified" ID="ID_1428889764" CREATED="1589248322715" MODIFIED="1589248346114"/>
</node>
</node>
<node TEXT="once started, fuseki listens to" ID="ID_1191188539" CREATED="1660923354821" MODIFIED="1660923370304">
<node TEXT="http://localhost:3030" ID="ID_301696471" CREATED="1660923371748" MODIFIED="1660923396147">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="Query with SPARQL" POSITION="right" ID="ID_1435806441" CREATED="1660913621857" MODIFIED="1660913649501">
<edge COLOR="#00ff00"/>
<node TEXT="write query" ID="ID_49214944" CREATED="1660922792452" MODIFIED="1660922797226">
<node TEXT="template" ID="ID_1811470901" CREATED="1660915144389" MODIFIED="1660915170220">
<icon BUILTIN="idea"/>
<node TEXT="https://gitlab.com/odameron/rdf-sparql-cheatsheet/-/raw/main/template.rq" ID="ID_1533104401" CREATED="1660915173188" MODIFIED="1660923657249">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="SPARQL syntax" ID="ID_340589656" CREATED="1660923666221" MODIFIED="1660923670247">
<node TEXT="prefixes" ID="ID_1813949706" CREATED="1660915270732" MODIFIED="1660915280714">
<node TEXT="PREFIX prefixName: &lt;prefixURL&gt;" ID="ID_769754362" CREATED="1660915313157" MODIFIED="1660924427437">
<font NAME="Courier" BOLD="true"/>
</node>
<node TEXT="usual prefixes" ID="ID_1015824210" CREATED="1660915433133" MODIFIED="1660916646788">
<icon BUILTIN="idea"/>
<node TEXT="http://prefix.cc" ID="ID_1192793701" CREATED="1660915493525" MODIFIED="1660915507333">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="variables" ID="ID_265592799" CREATED="1660924461761" MODIFIED="1660924464452">
<node TEXT="name starts with a question mark" ID="ID_1716973217" CREATED="1660924465553" MODIFIED="1660924476462"/>
<node TEXT="case sensitive" ID="ID_1973359425" CREATED="1660924479225" MODIFIED="1660924483422"/>
</node>
<node TEXT="SELECT ?variable1 ?variable2&#xa;WHERE {&#xa;  ...&#xa;}" ID="ID_1802185162" CREATED="1660924525745" MODIFIED="1660924601484">
<font NAME="Courier"/>
</node>
</node>
</node>
<node TEXT="alternative 1: run query directly on dataset" ID="ID_982846599" CREATED="1660922809891" MODIFIED="1660922842303">
<node TEXT="install apache jena" ID="ID_929452936" CREATED="1660916733237" MODIFIED="1660916738163">
<node TEXT="https://jena.apache.org/" ID="ID_1656792913" CREATED="1660916769319" MODIFIED="1660916783688">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="query local data" ID="ID_577677466" CREATED="1589243371758" MODIFIED="1589243376262">
<node TEXT="${JENA_HOME}/bin/sparql --data=/path/to/dataFile --query=/path/to/queryFile" ID="ID_437907219" CREATED="1589242140662" MODIFIED="1660923873016">
<icon BUILTIN="executable"/>
<font NAME="Courier" BOLD="true"/>
</node>
<node TEXT="use as many &quot;--data=file&quot; as necessary" ID="ID_233895734" CREATED="1589242289621" MODIFIED="1589242316068"/>
</node>
<node TEXT="query a remote endpoint" ID="ID_1941816986" CREATED="1589243377503" MODIFIED="1589243383996">
<node TEXT="${JENA_HOME}/bin/rsparql --service=endpointURL --query=/path/to/queryFile" ID="ID_1734226806" CREATED="1589243525814" MODIFIED="1660923928963">
<icon BUILTIN="executable"/>
<font NAME="Courier" BOLD="true"/>
</node>
</node>
</node>
<node TEXT="alternative 2: run query on some endpoint" ID="ID_1962631702" CREATED="1660922842979" MODIFIED="1660922861877">
<node TEXT="users: web interface" ID="ID_1257419674" CREATED="1589248551275" MODIFIED="1660924057698">
<node TEXT="http://localhost:3030" ID="ID_59198967" CREATED="1660923371748" MODIFIED="1660923396147">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
<node TEXT="programs: endpoint URL" ID="ID_894199623" CREATED="1589248445572" MODIFIED="1589248550826">
<node TEXT="http://localhost:3030/serviceName/query" ID="ID_692182413" CREATED="1589248459355" MODIFIED="1630145675132">
<font NAME="Courier"/>
</node>
<node TEXT="http://localhost:3030/serviceName/update" ID="ID_765404072" CREATED="1589248482628" MODIFIED="1630145684873">
<font NAME="Courier"/>
</node>
</node>
</node>
</node>
<node TEXT="Explore with rdfCartographer" POSITION="right" ID="ID_28256898" CREATED="1660913652057" MODIFIED="1660913662165">
<edge COLOR="#ff00ff"/>
<node TEXT="https://gitlab.com/odameron/rdfCartographer" ID="ID_1892871138" CREATED="1664360586914" MODIFIED="1664360613942">
<icon BUILTIN="internet"/>
<font NAME="Courier"/>
</node>
</node>
</node>
</map>
