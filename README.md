# rdf-sparql-cheatsheet

Cheat sheet for basic RDF and SPARQL usage.

See also [https://gitlab.com/odameron/rdf-sparql-tools](https://gitlab.com/odameron/rdf-sparql-tools) for more details, and [https://gitlab.com/odameron/fusekiInstallationUsage](https://gitlab.com/odameron/fusekiInstallationUsage) for step-by-step instructions for installing fuseki.
